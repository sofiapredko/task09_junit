package com.sofiatest;

import com.sofia.ServiceImplementation;
import com.sofia.SomeService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)

public class MockTest {
    @InjectMocks
    ServiceImplementation service;

    @Mock
    SomeService storeService;

    @Test
    public void testAdd() {
        when(storeService.add(15, 20)).thenReturn(35);
        assertEquals(service.add(15, 20), 35);
    }

    @Test
    public void testReject() {
        SomeService serv = Mockito.mock(SomeService.class);
        ServiceImplementation mathApp = new ServiceImplementation(serv);

        when(serv.reject(10, 5)).thenReturn(4);
        assertEquals(mathApp.reject(10, 5), 4);
    }

    @Test
    public void testOffer() {
        when(storeService.offer(30, 10)).thenReturn(25);
        assertEquals(service.offer(30, 10), 25);
        verify(storeService).offer(30, 10);
    }


}
