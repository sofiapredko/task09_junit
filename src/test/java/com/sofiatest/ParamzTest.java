package com.sofiatest;

import com.sofia.Flower;
import org.junit.jupiter.api.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(Parameterized.class)
public class ParamzTest {
    private static int count = 0;

    @Parameterized.Parameter(0)
    public int val1;

    @Parameterized.Parameter(1)
    public int val2;

    @Parameterized.Parameter(2)
    public int result;

    @Parameterized.Parameters(name = "{index}: testPrice ({0} * {1}) = {2}")
    public static Collection<Object[]> getTestData() {
        return Arrays.asList(new Object[][]{
                {1, 5, 5}, {2, 3, 6}, {10, 20, 200}, {1, 2, 2}
        });
    }

    @Test
    public void test() {
        count++;
        Flower fl = new Flower();
        fl.setPrice(val1);
        fl.setAmount(val2);
        int valueEnd = fl.getFlowersPrice();
        System.out.println(valueEnd);
        assertEquals(valueEnd, result);
    }

    @BeforeAll
    public static void beforeClass() {
        System.out.println("Count beforeClass is : " + count);
    }

    @BeforeEach
    public void beforeTest() {
        System.out.println("Count before is : " + count);
    }

    @AfterEach
    public void afterTest() {
        System.out.println("Count after is : " + count);
    }

    @AfterAll
    public static void afterClass() {
        System.out.println("Count AfterClass is : " + count);
    }

}
