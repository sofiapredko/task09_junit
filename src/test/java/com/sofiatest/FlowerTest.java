package com.sofiatest;

import java.io.IOException;

import com.sofia.Flower;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

public class FlowerTest {
    private static int count = 0;

    @BeforeAll
    public static void beforeClass() {
        System.out.println("Count beforeClass is : " + count);
    }

    @BeforeEach
    public void beforeTest() {
        System.out.println("Count before is : " + count);
    }

    @AfterEach
    public void afterTest() {
        System.out.println("Count after is : " + count);
    }

    @AfterAll
    public static void afterClass() {
        System.out.println("Count AfterClass is : " + count);
    }

    @Test
    public void getWarningTest(){
        count++;
        Flower fl1 = new Flower(5);
        fl1.setAmount(0);
        assertThrows(Exception.class, () -> fl1.getWarning() );
    }

    @Test
    public void buyMoreTest(){
        count++;
        Flower fl1 = new Flower();
        fl1.buyMore(2);
        int expected = 3;
        int current = fl1.getAmount();
        assertTrue(current == expected, "Result " + current + " != expected " + expected);
    }

    @Test
    public void getFlowersPriceTest(){
        count++;
        Flower fl1 = new Flower(10);
        fl1.buyMore(2);
        int priceAll = fl1.getFlowersPrice();
        int expected = 30;
        assertTrue(priceAll == expected, "Result " + priceAll + " != expected " + expected);
    }

    @Test
    @Disabled("Feature not implemented yet")
    public void getFlowerPotPriceTest(){
    }

    @Test
    public void getPriceForOneTest(){
        count++;
        Flower fl1 = new Flower(10);
        int current = fl1.getPriceForOne();
        int expected = 10;
        assertTrue(current == expected, "Result " + current + " != expected " + expected);
    }

    @Test
    public void getBouquetPriceTest(){
        count++;
        Flower fl1 = new Flower(10);
        fl1.buyMore(2);
        int priceBq= fl1.getBouquetPrice();
        int expected = 60;
        assertTrue(priceBq == expected, "Result " + priceBq + " != expected " + expected);
    }

    @Test
    public void getColourTest(){
        count++;
        Flower fl1 = new Flower("red");
        String current = fl1.getColor();
        String expected = "red";
        assertTrue(current.equals(expected), "Result " + current + " != expected " + expected);
    }


}
