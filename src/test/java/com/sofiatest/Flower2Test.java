package com.sofiatest;

import static org.junit.Assume.assumeNotNull;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

import com.sofia.Flower;
import org.junit.jupiter.api.*;

public class Flower2Test {
    private boolean condition=false;

    private static String message() {
        return "TEST Execution Failed :: ";
    }

    @Test
    void conditionTest() {
        condition = true;
        assumeTrue(condition);
        System.out.println("Test condition");
    }

    @Test
    public void assumeNotNullTest(){
        //Flower fl1 = null;
        Flower fl1 = new Flower();
        assumeNotNull(fl1);
    }

    @Test
    void failMethodTest() {
        //fail("Hello! :)");
        System.out.println("Test method fail");
    }


}
