import org.junit.platform.runner.JUnitPlatform;

import org.junit.platform.suite.api.IncludeClassNamePatterns;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.platform.suite.api.SuiteDisplayName;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(JUnitPlatform.class)
@SuiteDisplayName("JUnit 5 Suite Demo")
@SelectPackages("com.sofiatest")
@IncludeClassNamePatterns({"^.*$"})
public class AllSuiteTest {
}


