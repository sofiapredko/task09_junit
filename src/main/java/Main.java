import com.sofia.Flower;

public class Main {
    public static void main(String[] args) {
        Flower fl1 = new Flower();
        fl1.buyMore(2);
        fl1.setPrice(13);
        System.out.println(fl1.getFlowersPrice());
        System.out.println(fl1.getBouquetPrice());

        Flower fl2 = new Flower("red", 5, 12);
        Flower fl3 = new Flower("red", 5, 12);
        System.out.println(fl3.equals(fl2));
        fl2.setPrice(5);
        fl2.buyMore(10);
        fl3.buyMore(3);
        System.out.println(fl3.getColor());
        System.out.println(fl3.getAmount());
        System.out.println(fl2.getFlowersPrice());
        System.out.println(fl2.getBouquetPrice());

    }
}
