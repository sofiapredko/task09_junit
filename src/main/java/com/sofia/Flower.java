package com.sofia;

import java.util.Objects;

public class Flower {
    private String color;
    private int amount;
    private int price;

    public Flower() {
        this.color = "";
        this.price = 0;
        amount = 1;
    }

    public Flower(int price) {
        this.color = "";
        this.price = price;
        amount = 1;
    }

    public Flower(String color) {
        this.color = color;
        amount = 1;
    }

    public Flower(String color, int amount) {
        this.color = color;
        this.amount = amount;
    }

    public Flower(String color, int amount, int price) {
        this.color = color;
        this.amount = amount;
        this.price = price;
    }

    public int getFlowersPrice() {
        System.out.print("Price: ");
        return price * amount;
    }

    public int getBouquetPrice() {
        System.out.print("Price: ");
        return price * amount * 2;
    }

    public int buyMore(int n) {
        amount += n;
        return amount;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getPriceForOne() {
        return price;
    }

    public void getWarning() throws Exception {
        if (this.getAmount() == 0) throw new Exception("You buy 0 flowers? Really?");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flower flower = (Flower) o;
        return amount == flower.amount &&
                price == flower.price &&
                Objects.equals(color, flower.color);
    }

}
