package com.sofia;

public interface SomeService {
    int offer(int input1, int input2);
    int add(int input1, int input2);
    int reject(int input1, int input2);
}
