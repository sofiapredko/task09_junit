package com.sofia;

public class ServiceImplementation {
    private SomeService storeService;

    public ServiceImplementation() {
    }

    public ServiceImplementation(SomeService storeService) {
        this.storeService = storeService;
    }

    public void setSomeServiceImpl(SomeService storeService) {
        this.storeService = storeService;
    }

    public int offer(int input1, int input2) {
        int x = storeService.offer(input1, input2);
        return x;
    }

    public int reject(int input1, int input2) {
        return storeService.reject(input1, input2);
    }

    public int add(int input1, int input2) {
        return storeService.add(input1, input2);
    }


}
